# aurs

Very unoriginal project. Just learning some Rust basics by porting an existing Ruby script.

aurs can download packages, search packages, and look for updates on the AUR.
It can not *automatically* upgrade AUR packages.

## Building

    $ cargo build --release

## Testing

    $ cargo test

## Usage

Download a package:

    $ aurs -d qemu-patched

By default, this will save the package to `/data/AUR`. Either edit the code,
or change the `AURS_DOWNLOAD_PATH` environment variable at runtime to change this.

Search for packages (ordered by most votes first):

    $ aurs -s qemu

Print all available info for a give package:

    $ aurs -i qemu-patched

Look for updates to local foreign packages:

    $ aurs -u

## License

See LICENSE.
