const VERSION: Option<&str> = option_env!("CARGO_PKG_VERSION");
const AUR_URL: &str = "https://aur.archlinux.org";
const DEFAULT_DOWNLOAD_PATH: &str = "/data/AUR";

extern crate getopts; // cmdline argument parsing
extern crate serde;
#[macro_use] extern crate serde_derive;
extern crate serde_json; // json serialization
extern crate reqwest; // http requests
extern crate url; // url encoding
extern crate chrono; // time parsing

use std::env;
use std::process;

mod helpers;
use helpers::*;

#[derive(Serialize, Deserialize, Debug)]
pub struct AURResponse {
    version:     u32,
    resultcount: u32,
    results:     Vec<Package>
}

#[allow(non_snake_case)]
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Package {
    ID:             u32,
    Name:           String,
    PackageBaseID:  u32,
    PackageBase:    String,
    Version:        String,
    Description:    Option<String>,
    URL:            Option<String>,
    NumVotes:       u32,
    Popularity:     f64,
    OutOfDate:      Option<i64>,
    Maintainer:     Option<String>,
    FirstSubmitted: i64,
    LastModified:   i64,
    URLPath:        String,
    Depends:        Option<Vec<String>>,
    MakeDepends:    Option<Vec<String>>,
    OptDepends:     Option<Vec<String>>,
    Conflicts:      Option<Vec<String>>,
    Provides:       Option<Vec<String>>,
    Replaces:       Option<Vec<String>>,
    Groups:         Option<Vec<String>>,
    License:        Option<Vec<String>>,
    Keywords:       Option<Vec<String>>
}

fn download_package(package_name: &str) {
    print!("Downloading {}... ", package_name);

    match get_package_info(package_name) {
        Some(p) => {
            let mut download_path: String = DEFAULT_DOWNLOAD_PATH.to_string();

            match env::var("AURS_DOWNLOAD_PATH") {
                Ok(p) => {
                    download_path = p;
                },
                Err(_) => ()
            };

            assert!(env::set_current_dir(std::path::Path::new(&download_path)).is_ok());

            let ref url = format!("{}/{}.git", AUR_URL, p.Name);
            system("git", &vec!["clone", url]);

            println!("Done.")
        },
        None => println!("Package doesn't exist.")
    };
}

fn search_packages(term: &str) {
    let ref url = format!("{}/rpc/?v=5&type=search&arg={}", AUR_URL, urlencode(term));
    let mut response = match reqwest::get(url) {
        Ok(r)  => r,
        Err(e) => {
            eprintln!("Failed AUR RPC request: {}", e);
            process::exit(1);
        }
    };
    assert_eq!(reqwest::StatusCode::OK, response.status());

    let aur_response: AURResponse = match response.json() {
        Ok(j)  => j,
        Err(e) => {
            eprintln!("Failed parsing AUR JSON: {}", e);
            process::exit(1);
        }
    };

    if aur_response.resultcount == 0 {
        println!("No results found");
        return
    }

    let mut results: Vec<Package> = aur_response.results;
    results.sort_by_key(|k| k.NumVotes);
    results.reverse();

    for result in results {
        println!("  {} {} ({})",
                 ansi_color(&result.Name, Colors::Cyan),
                 ansi_color(&result.Version, Colors::Green),
                 result.NumVotes);
        println!("    {}",
                 result.Description.unwrap_or("No description".to_string()));
    }
}

fn print_package_info(package_name: &str) {
    match get_package_info(package_name) {
        Some(p) => {
            //println!("{:#?}", p);
            println!("{} {}", ansi_bold(&rjust_str("ID", 13)), p.ID);
            println!("{} {}", ansi_bold(&rjust_str("Name", 13)), p.Name);
            println!("{} {}", ansi_bold(&rjust_str("Version", 13)), p.Version);
            println!("{} {}",
                     ansi_bold(&rjust_str("Description", 13)),
                     p.Description.unwrap_or("No description".to_string()));
            match p.URL {
                Some(u) => println!("{} {}", ansi_bold(&rjust_str("URL", 13)), u),
                None    => ()
            };
            println!("{} {}", ansi_bold(&rjust_str("Votes", 13)), p.NumVotes);
            println!("{} {:.3}",
                     ansi_bold(&rjust_str("Popularity", 13)),
                     p.Popularity);
            match p.OutOfDate {
                Some(d) => println!("{} Flagged on {}",
                                    ansi_bold(&rjust_str("Out of date", 13)),
                                    epoch_to_datetime(d)),
                None    => ()
            };
            match p.Maintainer {
                Some(m) => println!("{} {}",
                                    ansi_bold(&rjust_str("Maintainer", 13)), m),
                None    => println!("{} None",
                                    ansi_bold(&rjust_str("Maintainer", 13)))
            };
            println!("{} {}",
                     ansi_bold(&rjust_str("Submitted", 13)),
                     epoch_to_datetime(p.FirstSubmitted));
            println!("{} {}",
                     ansi_bold(&rjust_str("Modified", 13)),
                     epoch_to_datetime(p.LastModified));
            match p.Depends {
                Some(d) => println!("{} {}",
                                    ansi_bold(&rjust_str("Depends", 13)),
                                    d.join(" ")),
                None    => ()
            };
            match p.MakeDepends {
                Some(d) => println!("{} {}",
                                    ansi_bold(&rjust_str("MakeDepends", 13)),
                                    d.join(" ")),
                None    => ()
            };
            match p.OptDepends {
                Some(d) => println!("{} {}",
                                    ansi_bold(&rjust_str("OptDepends", 13)),
                                    d.join(" ")),
                None    => ()
            };
            match p.Conflicts {
                Some(c) => println!("{} {}",
                                    ansi_bold(&rjust_str("Conflicts", 13)),
                                    c.join(" ")),
                None    => ()
            };
            match p.Provides {
                Some(p) => println!("{} {}",
                                    ansi_bold(&rjust_str("Provides", 13)),
                                    p.join(" ")),
                None    => ()
            };
            match p.Replaces {
                Some(r) => println!("{} {}",
                                    ansi_bold(&rjust_str("Replaces", 13)),
                                    r.join(" ")),
                None    => ()
            };
            match p.Groups {
                Some(g) => println!("{} {}",
                                    ansi_bold(&rjust_str("Groups", 13)),
                                    g.join(" ")),
                None    => ()
            };
            match p.License {
                Some(l) => println!("{} {}",
                                    ansi_bold(&rjust_str("License(s)", 13)),
                                    l.join(" ")),
                None    => ()
            };
            match p.Keywords {
                Some(k) => {
                    match k.len() {
                        0 => (),
                        _ => println!("{} {}",
                                      ansi_bold(&rjust_str("Keywords", 13)),
                                      k.join(" "))
                    }
                },
                None => ()
            };
        },
        None => println!("Package doesn't exist.")
    };
}

fn check_foreign_package_versions() {
    // Turn foo 1.2.3\nbar 1.2.3 into [["foo", "1.2.3"], ["bar", "1.2.3"]]
    let foreign_packages: Vec<Vec<String>> =
        system("pacman", &vec!["-Qm"]).split("\n").map(|s|
            s.to_owned().split(" ").map(|s| s.to_owned()
        ).collect()).collect();
    let foreign_package_names: Vec<String> = foreign_packages.iter().
        map(|p| urlencode(&p[0])).collect();

    let ref multiinfo_args: String = foreign_package_names.join("&arg[]=");
    let ref url = format!(
        "{}/rpc/?v=5&type=multiinfo&arg[]={}", AUR_URL, multiinfo_args
    );
    let mut response = match reqwest::get(url) {
        Ok(r)  => r,
        Err(e) => {
            eprintln!("Failed AUR RPC request: {}", e);
            process::exit(1);
        }
    };
    assert_eq!(reqwest::StatusCode::OK, response.status());

    let aur_response: AURResponse = match response.json() {
        Ok(j)  => j,
        Err(e) => {
            eprintln!("Failed parsing AUR JSON: {}", e);
            process::exit(1);
        }
    };

    for result in aur_response.results {
        let ref local_info =
            match foreign_packages.iter().find(|&p| p[0] == result.Name) {
                Some(i) => i,
                None => {
                    eprintln!("Couldn't find local package for {}", result.Name);
                    continue;
                }
            };
        let ref local_version = local_info[1];

        if compare_version(local_version, &result.Version) == -1 {
            println!("{} {} has an update available ({} -> {})",
                     ansi_color(">", Colors::Yellow),
                     ansi_color(&result.Name, Colors::Cyan),
                     ansi_color(&local_version, Colors::Red),
                     ansi_color(&result.Version, Colors::Green));
        } else {
            println!("  {} {} is up to date",
                     ansi_color(&result.Name, Colors::Cyan),
                     local_version);
        }
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let ref program = args[0];

    let mut options = getopts::Options::new();
    options.optopt( "d", "download", "download given package", "PKG");
    options.optopt( "s", "search",   "search for given search term", "TERM");
    options.optopt( "i", "info",     "print information of given package", "PKG");
    options.optflag("u", "upgrades", "check for updates to foreign packages");
    options.optflag("h", "help",     "print this help menu");
    options.optflag("v", "version",  "print program version");

    let matches = match options.parse(&args[1..]) {
        Ok(m)  => m,
        Err(e) => {
            eprintln!("{}", e);
            process::exit(1);
        }
    };

    if matches.opt_present("h") {
        print_usage(program, &options);
    }

    if matches.opt_present("v") {
        println!("v{}", VERSION.unwrap_or("unknown"));
        process::exit(0);
    }

    if matches.opt_present("d") {
        let ref package_to_download = match matches.opt_str("d") {
            Some(p) => p,
            None => {
                eprintln!("No package specified");
                process::exit(1);
            }
        };

        download_package(package_to_download);
        process::exit(0);
    }

    if matches.opt_present("s") {
        let ref search_term = match matches.opt_str("s") {
            Some(t) => t,
            None => {
                eprintln!("No search term specified");
                process::exit(1);
            }
        };

        search_packages(search_term);
        process::exit(0);
    }

    if matches.opt_present("i") {
        let ref package_to_query = match matches.opt_str("i") {
            Some(p) => p,
            None => {
                eprintln!("No package specified");
                process::exit(1);
            }
        };

        print_package_info(package_to_query);
        process::exit(0);
    }

    if matches.opt_present("u") {
        check_foreign_package_versions();
        process::exit(0);
    }

    if matches.free.is_empty() {
        print_usage(program, &options);
    }
}
