use std::process;

#[allow(dead_code)]
pub enum Colors {
    Grey    = 30,
    Red     = 31,
    Green   = 32,
    Yellow  = 33,
    Blue    = 34,
    Magenta = 35,
    Cyan    = 36,
    White   = 37
}

pub fn ansi_bold(text: &str) -> String {
    format!("\x1b[0;1m{}\x1b[0m", text)
}

pub fn ansi_color(text: &str, color: Colors) -> String {
    format!("\x1b[0;{}m{}\x1b[0m", color as u16, text)
}

pub fn rjust_str(text: &str, length: i16) -> String {
    let spaces: i16 = length - text.len() as i16;
    if spaces < 0 {
        return format!("{}", text)
    }

    let mut s = String::new();
    let mut i = 0;
    while i < spaces {
        s.push(' ');
        i = i + 1;
    }

    format!("{}{}", s, text)
}

pub fn urlencode(string: &str) -> String {
    ::url::form_urlencoded::byte_serialize(string.as_bytes()).collect()
}

pub fn epoch_to_datetime(epoch: i64) -> String {
    format!("{}", ::chrono::prelude::NaiveDateTime::from_timestamp(epoch, 0))
}

pub fn system(command: &str, args: &Vec<&str>) -> String {
    match process::Command::new(&command).args(args).output() {
        Ok(o)  => String::from_utf8(o.stdout).unwrap().trim().to_owned(),
        Err(e) => panic!("Command '{}' failed because {:?}", command, e)
    }
}

pub fn compare_version(v1: &str, v2: &str) -> i8 {
    system("vercmp", &vec![v1, v2]).parse::<i8>().expect("vercmp failed")
}

pub fn print_usage(program: &str, options: &::getopts::Options) {
    let ref brief = format!("Usage: {} [options]", program);

    println!("{}", options.usage(brief));
    process::exit(0);
}

pub fn get_package_info(package_name: &str) -> Option<::Package> {
    let ref url = format!("{}/rpc/?v=5&type=info&arg={}", ::AUR_URL, urlencode(package_name));
    let mut response = match ::reqwest::get(url) {
        Ok(r)  => r,
        Err(e) => {
            eprintln!("Failed AUR RPC request: {}", e);
            process::exit(1);
        }
    };
    assert_eq!(::reqwest::StatusCode::OK, response.status());

    //let json: serde_json::Value = serde_json::from_str(&body).unwrap();
    //let package: Package = serde_json::from_str(&serde_json::to_string(&json["results"][0]).unwrap()).unwrap();
    let aur_response: ::AURResponse = match response.json() {
        Ok(j)  => j,
        Err(e) => {
            eprintln!("Failed parsing AUR JSON: {}", e);
            process::exit(1);
        }
    };

    if aur_response.resultcount == 0 {
        return None
    }

    Some(aur_response.results[0].clone()) // Can't just borrow out of this lifetime
}

//----------------------------------------------------------------------------

#[test]
fn test_compare_version() {
    assert_eq!(-1, compare_version("1.0", "1.1"));
    assert_eq!(0,  compare_version("1.0", "1.0"));
    assert_eq!(1,  compare_version("1.1", "1.0"));
}

#[test]
fn test_get_package_info() {
    let package = get_package_info("qemu-patched").unwrap();

    assert_eq!("qemu-patched", package.Name);
    assert_eq!(487725, package.ID);
    assert_eq!(126845, package.PackageBaseID);
    assert_eq!("/cgit/aur.git/snapshot/qemu-patched.tar.gz", package.URLPath);

    let package = get_package_info("asdfsdfasdfasdfsfa");
    assert!(package.is_none());
}

#[test]
fn test_ansi_color() {
    assert_eq!("\x1b[0;32mTest\x1b[0m", &ansi_color("Test", Colors::Green));
}

#[test]
fn test_rjust_str() {
    assert_eq!("test", &rjust_str("test", 3));
    assert_eq!("  t", &rjust_str("t", 3));
}
